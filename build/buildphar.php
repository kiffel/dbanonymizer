<?php

$phar = new Phar("DbAnonymizer.phar", FilesystemIterator::CURRENT_AS_FILEINFO | FilesystemIterator::KEY_AS_FILENAME, "DbAnonymize.phar");

$phar->buildFromDirectory("../thirdparties");
$phar["index.php"] = file_get_contents("../index.php");
$phar->setStub($phar->createDefaultStub("index.php"));
