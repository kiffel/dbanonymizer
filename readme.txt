--- Goal

This tools is designed to anonymize data in database with random value.
Data concerned are names, phone numbers, bank or credit cards ids, ...
This is useful when you want to convert a production db with sensible data to dev.

This tools is using Faker : https://github.com/fzaninotto/Faker

--- HowTo

Create a file called "config.json" with the following json structure : 

{
	"database":{
		"host":"YOUR HOST",
		"db":"DB NAME",
		"user":"USER",
		"pass":"MYSQL PASSWORD",
		"driver":"pdo_mysql"
	},
	"fake": [
		{
			"table":"TABLENAME",
			"keys":["PK_FIELD1", ...],
			"fields" : [
						{"name":"accomName", "formatter":"name"},
						...
					]
		},		
		...
	]
}


--- More information on json configuration options

- You can add as many table as you want in fake section
- You can add as many fields as you want
- Only numerical keys are handled (i.e. keys filter are not quoted in sql queries)
- optional and unique field attributes are considered false if ommited
- You can use a specific "maxRecords" attribute in top level properties of json to limit number of updates per table


--- More information on available Faker formatter

name
firstName
lastName
citySuffix
streetSuffix
buildingNumber
city
streetName
streetAddress
postcode
address
country

latitude
longitude

ean13
ean8

phoneNumber

company
companySuffix

creditCardType
creditCardNumber
creditCardExpirationDate
creditCardExpirationDateString
creditCardDetails
bankAccountNumber

word

email
safeEmail
freeEmail
companyEmail
freeEmailDomain
safeEmailDomain
userName
domainName
domainWord
tld
url
ipv4
ipv6
internalIpv4
macAddress

unixTime
dateTime
dateTimeAD
iso8601
dateTimeThisCentury
dateTimeThisDecade
dateTimeThisYear
dateTimeThisMonth
amPm
dayOfMonth
dayOfWeek
month
monthName
year
century
timezone

md5
sha1
sha256
locale
countryCode
languageCode

randomDigit
randomDigitNotNull
randomLetter

userAgent
chrome
firefox
safari
opera
internetExplorer

uuid

mimeType
fileExtension

hexcolor
safeHexColor
rgbcolor
rgbColorAsArray
rgbCssColor
safeColorName
colorName


--- Complete Example : 

{	
	globalization:"en_GB",
	"database":{
		"host":"localhost",
		"db":"DB",
		"user":"USERNAME",
		"pass":"PASSWORD",
		"driver":"pdo_mysql"
	},
	"fake": [
		{
			"table":"Accom_Payments",
			"keys":["Accom_Payments_ID"],
			"fields" : [
						{"name":"accomName", "formatter":"name"},
						{"name":"accomFirstName", 	"formatter":"firstname"},
						{"name":"studentName", 		"formatter":"lastName"}
					]
		},
		{
			"table":"Agent",
			"keys":["A_ID"],
			"fields" : [
						{"name":"A_name", "formatter":"company"},
						{"name":"A_abbreviation", "formatter":"word", "optional":"true"},
						{"name":"A_address0", "formatter":"streetAddress"},
						{"name":"A_address1", "formatter":"postcode"},
						{"name":"A_address2", "formatter":"city"},
						{"name":"A_phone", "formatter":"phoneNumber", "optional":"true"},
						{"name":"A_phone2", "formatter":"phoneNumber", "optional":"true"},
						{"name":"A_fax", "formatter":"phoneNumber", "optional":"true"},
						{"name":"A_fax2", "formatter":"phoneNumber", "optional":"true"},
						{"name":"A_mail", "formatter":"safeEmail", "optional":"true"},
						{"name":"A_misc", "formatter":"word", "optional":"true"},
						{"name":"A_url", "formatter":"url", "optional":"true"},
						{"name":"invoice_address", "formatter":"address", "optional":"true"},
						{"name":"account_holder", "formatter":"company", "optional":"true"},
						{"name":"account_number", "formatter":"bankAccountNumber", "optional":"true"},
						{"name":"bank_address", "formatter":"address", "optional":"true"},
						{"name":"swift_code", "formatter":"randomNumber", "optional":"true"},
						{"name":"iban_code", "formatter":"bankAccountNumber", "optional":"true"},
						{"name":"cas_comment", "formatter":"word", "optional":"true"}
						]
		},
		{
			"table":"Agent_Contacts",
			"keys":["Agent_Contacts_ID"],
			"fields" : [
						{"name":"name", "formatter":"name"},
						{"name":"firstname", "formatter":"firstName"},
						{"name":"familyname", "formatter":"lastName"},
						{"name":"middlename", "formatter":"firstName", "optional":"true"},
						{"name":"title", "formatter":"title", "optional":"true"},
						{"name":"phone", "formatter":"phoneNumber", "optional":"true"},
						{"name":"homePhone", "formatter":"phoneNumber", "optional":"true"},
						{"name":"mobilePhone", "formatter":"phoneNumber", "optional":"true"},
						{"name":"email", "formatter":"safeEmail"},
						{"name":"comment", "formatter":"word", "optional":"true"}
						]
		},
		{
			"table":"Homestay",
			"keys":["H_ID"],
			"fields" : [
						{"name":"H_familyName0", "formatter":"lastName"},
						{"name":"H_familyName1", "formatter":"lastName", "optional":"true"},
						{"name":"H_firstName0", "formatter":"firstName"},
						{"name":"H_firstName1", "formatter":"firstName", "optional":"true"},
						{"name":"H_address", "formatter":"streetAddress"},
						{"name":"H_homePhone", "formatter":"phoneNumber", "optional":"true"},
						{"name":"H_mobilePhone0", "formatter":"phoneNumber","optional":"true"},
						{"name":"H_mobilePhone1", "formatter":"phoneNumber","optional":"true"},
						{"name":"H_workPhone0", "formatter":"phoneNumber","optional":"true"},
						{"name":"H_mail0", "formatter":"safeEmail","optional":"true"},
						{"name":"account_name", "formatter":"lastName","optional":"true"},
						{"name":"account_number", "formatter":"bankAccountNumber","optional":"true"},
						{"name":"account_name", "formatter":"lastName","optional":"true"},
						{"name":"H_other", "formatter":"word","optional":"true"},
						{"name":"H_address_2", "formatter":"streetSuffix","optional":"true"},
						{"name":"H_address_3", "formatter":"postcode","optional":"true"},
						{"name":"city", "formatter":"city"},
						{"name":"contactDetails", "formatter":"word", "optional":"true"},
						{"name":"H_templateLetter", "formatter":"word"}
						]
		},

		{
			"table":"Homestay_Children",
			"keys":["HC_ID"],
			"fields" : [
						{"name":"HC_name", "formatter":"firstName"}
						]
		},

		{
			"table":"L_Teacher",
			"keys":["TEA_ID"],
			"fields" : [
						{"name":"TEA_FNAME", "formatter":"name"}
					]
		},

		{
			"table":"Student_OwnAccom_Address",
			"keys":["id"],
			"fields" : [
						{"name":"address", "formatter":"streetAddress"},
						{"name":"postcode", "formatter":"postcode"},
						{"name":"phone", "formatter":"phoneNumber"}
					]
		},

		{
			"table":"user",
			"keys":["id"],
			"whereFilter":"id <> 1",
			"fields" : [
						{"name":"first_name", "formatter":"firstName"},
						{"name":"last_Name", "formatter":"lastName"},
						{"name":"username", "formatter":"lastName", "unique":"true"},
						{"name":"email", "formatter":"safeEmail", "unique":"true"}
					]
		},

		{
			"table":"Student",
			"keys":["S_ID"],
			"fields" : [
						{"name":"S_familyName", "formatter":"name"},
						{"name":"S_firstName", 	"formatter":"firstname"},
						{"name":"address", 		"formatter":"streetAddress"},
						{"name":"postcode", 		"formatter":"postcode"},
						{"name":"city", 		"formatter":"city"},
						{"name":"homephone", 		"formatter":"phoneNumber", "optional":"true"},
						{"name":"fax", 		"formatter":"phoneNumber", "optional":"true"},
						{"name":"email", 		"formatter":"safeEmail", "optional":"true"},
						{"name":"S_mobilePhone", "formatter":"phonenumber", "optional":"true"},
						{"name":"company", "formatter":"company", "optional":"true"}
					]
		}
	]
}
