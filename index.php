<?php
require_once "phar://DbAnonymizer.phar/vendor/autoload.php";

$config = parseConfig();
if (is_null($config) == FALSE)
    anonymize($config);

function parseConfig()
{

    $config = json_decode(file_get_contents("config.json"));

    $parseOk = FALSE;

    switch (json_last_error()) 
    {
            case JSON_ERROR_NONE:

                $parseOk = TRUE;
            break;
            case JSON_ERROR_DEPTH:
                echo "\n Max depth reached\n";
            break;
            case JSON_ERROR_STATE_MISMATCH:
                echo "\n underflow error\n";
            break;
            case JSON_ERROR_CTRL_CHAR:
                echo "\n charset control error\n";
            break;
            case JSON_ERROR_SYNTAX:
                echo "\n parse error\n";
            break;
            case JSON_ERROR_UTF8:
                echo "\n UTF-8 encoding error\n";
            break;
            default:
                echo "\n unknown error\n";
            break;
    }

    if ($parseOk == FALSE)
        $config = null;

    return $config;
}


function anonymize($config) {

    // use the factory to create a Faker\Generator instance
    $faker = Faker\Factory::create($config->globalization);

    if (isset($config->maxRecords)==false)
        $maxRecords = -1;
    else
        $maxRecords = $config->maxRecords;

    echo "DbAnonymizer application is now running ... \n";

    $dbalConf = new \Doctrine\DBAL\Configuration();
    $connectionParams = array(
        'dbname' => $config->database->db,
        'user' => $config->database->user,
        'password' => $config->database->pass,
        'host' => $config->database->host,
        'driver' => $config->database->driver
    );
    $conn = \Doctrine\DBAL\DriverManager::getConnection($connectionParams, $dbalConf);

    foreach ($config->fake as $fake)
    {
        $table = $fake->table;
        $whereFilter = '1=1';
        if (isset($fake->whereFilter))
            $whereFilter = $fake->whereFilter;

        echo "Updating $table ...";

        $keys = implode(',',$fake->keys);

        $sql    = "SELECT $keys FROM $table WHERE $whereFilter";
        $stmt = $conn->query($sql);

        $nbRecords = 0;
        
        while ($row = $stmt->fetch()) {

            $nbRecords++;
            if ($maxRecords != -1 && $nbRecords > $maxRecords)
            {
                $nbRecords--;
                break;
            }

            $error = false;
            $newFieldValuesArray = array();
            foreach ($fake->fields as $field)
            {
                $name = $field->name;
                $formatter = $field->formatter;
                try
                {
                    if (isset($field->optional) && $field->optional == "true")
                        $newVal = $faker->optional()->$formatter;
                    else if (isset($field->unique) && $field->unique == "true")
                        $newVal = $faker->unique()->$formatter;
                    else
                        $newVal = $faker->$formatter;
                }
                catch (Exception $e)
                {
                    echo 'Error while generation random data : ' . $e->getMessage() . "\n";
                    $error = true;
                }
                $newFieldValuesArray[$name]  = $newVal;
            }
            
            if ($error)
                break;

            $pkFilterArray = array();
            foreach ($fake->keys as $key)
            {
                $pkFilterArray[$key] = $row[$key];
            }

            $conn->update($table,$newFieldValuesArray,$pkFilterArray);

            echo '.';
        }

        if (isset($config->sqlOnly) == false || $config->sqlOnly != "true")
        {
            echo "\n $nbRecords rows updated in $table with random data.\n";
        }
    }
 }